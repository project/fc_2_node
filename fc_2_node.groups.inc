<?php

/**
 * Builds group based on field data
 */
function _fc_2_node_create_group($group_bundle_data) {

  if ( $group_bundle_data ) {
    drupal_set_message(t('Migrating field groups.'));
  }

  foreach($group_bundle_data as $group) {
    //Create Group for Item
    $group_data = (object) unserialize($group['data']);
    $group_data->entity_type = 'node';
    $group_data->mode = 'form';
    $group_data->identifier = $group['group_name']."|node|".str_replace('field_','',$group['bundle'])."|form";
    $group_data->bundle = str_replace('field_','',$group['bundle']);
    $group_data->group_name = $group['group_name'];

    if(!empty($group['parent'])) {
      $group_data->parent_name = $group['parent'];
    }

    //Check to see if group with data exsists... if not create it.
    $query = db_select('field_group', 'fg')
      ->condition('fg.identifier', $group_data->identifier, '=')
      ->fields('fg', array('identifier'));
    $result = $query->execute()->fetchAssoc();

    if(empty($result)){
      //Create group with object data
      $db_group_record = field_group_group_save($group_data);

      if( $db_group_record ) {
        drupal_set_message(t('Created @sub@group Successfully.',
          array( '@sub'=>(!empty($group['parent']) ? 'Sub ' : ''), '@group' => $group['group_name'])));
      }
    }
    else {
      drupal_set_message(t('Skipped @sub@group because it was empty.',
        array( '@sub'=>(!empty($group['parent']) ? 'Sub ' : ''), '@group' => $group['group_name'])));
    }
  }
}

/**
 * Build Groups to conversion
 */
function _fc_2_node_build_groups_data() {

  // cache it
  static $group_data;
  if ( ! is_array($group_data) ) {
    $group_data = array();

    //Get group information
    $fcs_groups = db_query(
      "SELECT group_name,bundle,data FROM field_group WHERE entity_type = 'field_collection_item'")
      ->fetchAll();

    //Build out arrays of content
    foreach($fcs_groups as $group){
      $group_data[$group->group_name] = unserialize($group->data);
      $group_data[$group->group_name]['data'] = $group->data;
      $group_data[$group->group_name]['bundle'] = $group->bundle;
      $group_data[$group->group_name]['group_name'] = $group->group_name;
    }

    //Build out temp arrays of content
    foreach($fcs_groups as $group){

      $data = unserialize($group->data);
      if(!empty($data['children'])){
        foreach($data['children'] as $child){
          if(substr($child,0,6) === "group_"){
             //Found group childen... add parent to child array
            $group_data[$child]['parent'] = $group->group_name;
          }
        }
      }
    }
  }
  return $group_data;
}

/**
 * Search for value inside mulitdimensional array
 * Used for groups migration
 */
function _fc_2_node_search_array($array, $key, $value){
  $results = array();
  if (is_array($array)) {
    if (isset($array[$key]) && $array[$key] == $value) {
      $results[] = $array;
    }
    foreach ($array as $subarray) {
      $results = array_merge($results, _fc_2_node_search_array($subarray, $key, $value));
    }
  }
  return $results;
}
