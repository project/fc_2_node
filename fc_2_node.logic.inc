<?php

/**
 * Migrates a field collection to a nodes
 */
function _fc_2_node_migrate_fc($fc_name, $options = array()) {

  // this stanza prevents FCs from being processed twice,
  // when they are both selected in the UI and also a parent FC
  if ( in_array($fc_name, _fc_2_node_processed()) ) {
  // this FC has already been processed.
    return;
  }

  drupal_set_message(t('Processing field collection \'@fcn\'.',
    array('@fcn'=> $fc_name)));

  // process host FCs first, if they exist.
  if ( count( $host_fcs = _fc_2_node_get_host_fcs($fc_name)) ) {
    foreach ( $host_fcs as $host ) {
      drupal_set_message(t('The field collection \'@fc\' has a host field collection \'@host\'.'
        . ' This must be migrated first.', array('@fc'=>$fc_name, '@host'=>$host)));
      _fc_2_node_migrate_fc($host, $options);
    }
  }

  // create the new content type
  module_load_include('inc', 'fc_2_node', 'fc_2_node.content_type');
  $new_ct = _fc_2_node_create_new_content_type($fc_name);

  // migrate field groups if they exist
  if (module_exists('field_group')) {
    module_load_include('inc', 'fc_2_node', 'fc_2_node.groups');
    $group_data = _fc_2_node_build_groups_data();
    $group_bundle_data = _fc_2_node_search_array($group_data, 'bundle', $fc_name);
    _fc_2_node_create_group($group_bundle_data);
  }

  // select field collections
  // load all instances of the field collection entity
  $result = _fc_2_node_get_fc_ids($fc_name);
  $count = count($result);

  module_load_include('inc', 'fc_2_node', 'fc_2_node.node');

  // copy the field values from the field collection to the new content type node
  $fcs = entity_load('field_collection_item', array_keys($result));
  foreach ( $fcs as $fc ) {
    // migrate the FC to a node
    $node_wrapper = _fc_2_node_add_node($new_ct->type, $fc_name, $fc, $options);
  }

  // add this to the list of fc's processed
  _fc_2_node_processed($fc_name);

  drupal_set_message(t('Migrated @count field collections of bundle \'@bundle\'.',
     array('@count' => $count, '@bundle' => $fc_name)));
}

/**
 * Gets the ids of the field collection of the specified type
 */
function _fc_2_node_get_fc_ids($fc_name) {

  $query = db_select('field_collection_item', 'fci')
    ->fields('fci', array('field_name', 'item_id'))
    ->condition('fci.field_name', $fc_name)
  ;

  $result = $query->execute()->fetchAllAssoc('item_id');
  return $result;
}

/**
 * A logging function to keep track of which field collections have been processed
 */
function _fc_2_node_processed($fc = NULL) {
  static $processed = array();
  if ( ! is_null($fc) ) {
    $processed[] = $fc;
  }
  return $processed;
}

/**
 * A function to keep track of if verbosity is set
 */
function _fc_2_node_verbose($new_setting = NULL) {
  static $setting;
  if ( is_bool($new_setting) ) {
    $setting = $new_setting;
  }
  if ( isset($setting) ) {
    return $setting;
  }
}

/**
 * returns host field collections, if any exist.
 */
function _fc_2_node_get_host_fcs($fc_name) {
  $fcs = field_info_field_map();
  if ( isset($fcs[$fc_name]['bundles']['field_collection_item']) ) {
    return $fcs[$fc_name]['bundles']['field_collection_item'];
  }
  else {
    return array();
  }
}

/**
 * Deletes field collection items, and FC parents
 */
function _fc_2_node_delete_fc_items($fc_name) {
  $result = _fc_2_node_get_fc_ids($fc_name);
  $count = count($result);
  entity_delete_multiple('field_collection_item', array_keys($result));
  $msg = 'Deleted @count field collections of bundle \'@bundle\'.';
  drupal_set_message(t($msg, array('@count' => $count, '@bundle' => $fc_name)));
  // find if this has any host FCs that need deletion also
  if ( count( $host_fcs = _fc_2_node_get_host_fcs($fc_name)) ) {
    foreach ( $host_fcs as $host ) {
      _fc_2_node_delete_fc_items($host);
    }
  }
}
