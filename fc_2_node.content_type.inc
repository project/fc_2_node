<?php

/**
 * Creates a new content type from the specified field collection
 */
function _fc_2_node_create_new_content_type($fc_name) {

  $new_ct_info = _fc_2_node_prep_fc_fields($fc_name);
  // Create New Content Type
  $new_ct = (object) array(
    'type' => $new_ct_info['content_type'],
    'orig_type' => $new_ct_info['content_type'],
    'base' => 'node_content',
    'name' => $new_ct_info['ct_name'],
    'description' => t('Replaces @ct_name field collection.',
      array('@ct_name' => $new_ct_info['ct_name'])),
    'locked' => TRUE,
    'custom' => TRUE,
    'disabled' => FALSE,
    'has_title' => TRUE,
    'title_label' => 'Title',
    'module' => 'node',
  );

  node_type_save($new_ct);
  drupal_set_message(t('Created new content type \'@new\' from field collection \'@ct\'.',
    array('@new'=>$new_ct_info['ct_name'], '@ct'=>$fc_name)));

  // attach the field collection fields to the new content type
  _fc_2_node_copy_fc_field_instances_to_content_type($fc_name, $new_ct);

  $host_entities = _fc_2_node_get_fc_host_entities($fc_name);
  foreach ( $host_entities as $entity_type => $bundles ) {
    foreach ( $bundles as $bundle ) {
      _fc_2_node_add_er_field($entity_type, $target_bundle = $bundle, $source_bundle = $new_ct->type);
    }
  }

  return $new_ct;
}

/**
 * Copy fields from a field collection to a content type
 */
function _fc_2_node_copy_fc_field_instances_to_content_type($fc, $content_type) {

  // get the fields for the specified field_collection
  $fc_fields = _fc_2_node_get_fc_fields($fc);

  // find out what fields this content type already has.
  $content_type_instances_info = _fc_2_node_get_node_bundle_fields($content_type->type);

  // catch all the field migrations to push them into a single update message
  $attached_fields = array();
  $existing_fields = array();

  // attach each field to the passed content type
  foreach ( $fc_fields as $field_name => $fc_field_instance ) {

    // create a new instance of the field on the new content type

    // stub it out from the field collection instance
    $new_content_type_field_instance = $fc_field_instance;

    // change it to the proper entity type and bundle
    $new_content_type_field_instance['entity_type'] = "node";
    $new_content_type_field_instance['bundle'] = $content_type->type;

    // checking for the field already existing on the content type was Too Hard(tm)
    // It will throw an exception if the field already exists.
    // Check for that exception and re-throw others
    try {
      $new_instance = field_create_instance($new_content_type_field_instance);
      $attached_fields[] = $field_name;
    } catch ( exception $e ) {
      // no code number to identify exception. Look in the error message.
      if ( strstr($e->getMessage(), "that already has an instance of that field.") ) {
        $existing_fields[] = $field_name;
      } else {
        throw $e;
      }
    }

  }
  if ( $attached_fields ) {
    drupal_set_message(t('Attached field(s) \'@field\' to content type \'@content\'.',
      array('@field'=>implode("', '", $attached_fields),'@content'=>$content_type->type)));
  }
  if ( $existing_fields ) {
    drupal_set_message(t('Content Type \'@content\' already has field(s) \'@field\'.',
      array('@field'=>implode("', '", $existing_fields),'@content'=>$content_type->type)));
  }
}

/**
 * return the field name and instances of the passed field collection
 */
function _fc_2_node_get_fc_fields($fc) {
  $fc_fields = field_info_instances('field_collection_item', $fc);

  foreach ( $fc_fields as $fc_name => $instance ) {
    $output[$fc_name] = $instance;
  }

  return $output;
}

/**
 * return the field name and instances of the passed field collection
 */
function _fc_2_node_get_node_bundle_fields($bundle) {

  $output = array();

  $node_bundles = field_info_instances('node');
  foreach ( $node_bundles as $bundle_name => $fields ) {
    if ( $bundle_name == $bundle ) {
      foreach ( $fields as $field => $field_instance ) {
        $output[$bundle_name] = $field;
      }
    }
  }

  return $output;
}

/**
 * Adds an entity relationship field to the specified bundle
 * @parameter $target_bundle
 *   The bundle that the source bundle should be attached to.
 * @parameter $source_bundle
 *   The string name of the migrated field collection bundle
 */
function _fc_2_node_add_er_field($entity_type, $target_bundle, $source_bundle) {

  $field_name = $source_bundle.'_er';
  $field_label = strtr($source_bundle, '_', ' ');

  // create the entity relationship field if it doesn't already exist.
  if (!field_info_field($field_name)) {

    $field = _fc_2_node_get_entity_reference_field_array($field_name, $source_bundle);

    try {
      $field = field_create_field($field);
    } catch ( exception $e ) {
      drupal_set_message('Caught exception @code \'@message\' while trying to create field \'@field\'',
        array('@message'=>$e->getMessage(), '@code'=>$e->getCode(), '@field'=>$field_name));
    }
    if ( ! $field ) {
      drupal_set_message(t('Could not create entity relationship field @field.',
        array('@field' => $field_name)));
    }
    else {
      drupal_set_message(t('Created entity relationship field @field.',
        array('@field' => $field_name)));
    }
  }

  // field_collection_item entity types can't have entity reference fields attached to them :(
  // throws exception "Attempt to create an instance of field {field name} on forbidden entity type field_collection_item."
  if ( $entity_type != 'field_collection_item' ) {
    $field_info_instance = field_info_instance($entity_type, $field_name, $target_bundle);

    if ( ! $field_info_instance ) {

      // attach an instance to the target bundle
      $instance = _fc_2_node_get_entity_reference_field_instance_array(
        $entity_type, $target_bundle, $field_name, $field_label);

      try {
        field_create_instance($instance);
        drupal_set_message(t('Attached entity reference field \'@label\' (@name) to bundle \'@bundle\'.',
          array('@name'=>$field_name, '@label'=>$field_label, '@bundle'=>$target_bundle)));
      }
      catch ( Exception $e ) {
        drupal_set_message(t('Could not attach entity reference \'@label\' (@name) to bundle \'@bundle\';'
            . ' caught exception @code \'@message\'.',
          array('@name'=>$field_name, '@label'=>$field_label, '@bundle'=>$target_bundle,
            '@code'=>$e->getCode(), '@message'=>$e->getMessage())));
      }
    }
  }
}

/**
 * Returns the array defining the entity reference field instance
 */
function _fc_2_node_get_entity_reference_field_instance_array($entity_type, $bundle, $field_name, $field_label) {
  $instance = array(
    'field_name' => $field_name,
    'entity_type' => $entity_type,
    'bundle' => $bundle,
    'label' => 'Linked '.$field_label,
    'required' => TRUE,
    'widget' => array(
      'weight' => '42',
      'type' => 'inline_entity_form',
      'module' => 'inline_entity_form',
      'active' => 1,
      'settings' => array(
        'fields' => array(),
        'type_settings' => array(
          'allow_existing' => 0,
          'match_operator' => 'CONTAINS',
          'delete_references' => 0,
          'override_labels' => 0,
          'label_singular' => 'node',
          'label_plural' => 'nodes',
        ),
      ),
    ),
    'settings' => array(
      'user_register_form' => false,
    ),
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'type' => 'entityreference_entity_view',
        'settings' => array(
          'view_mode' => 'default',
          'link' => false,
        ),
        'module' => 'entityreference',
        'weight' => 3,
      ),
      'full' => array(
        'label' => 'hidden',
        'type' => 'entityreference_entity_view',
        'settings' => array(
          'view_mode' => 'full',
          'link' => false,
        ),
        'module' => 'entityreference',
        'weight' => 3,
      ),
    ),
    'required' => 0,
    'description' => '',
    'ds_extras_field_template' => '',
  );
  return $instance;
}

/**
 * Returns the array defining the entity reference field
 */
function _fc_2_node_get_entity_reference_field_array($field_name, $target_bundle) {
  $field = array(
    'field_name' => $field_name,
    'type' => 'entityreference',
    'cardinality' => -1,
    'translatable' => '1',
    'entity_types' => array(
      'node',
    ),
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(
      'node' => array(
        'table' => 'node',
        'columns' => array(
          'target_id' => 'nid',
        ),
      ),
    ),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'settings' => array(
      'target_type' => 'node',
      'handler' => 'base',
      'handler_settings' => array(
        'target_bundles' => array($target_bundle),
      ),
    ),
    'storage' => array(
      'type' => 'field_sql_storage',
      'settings' => array(),
      'module' => 'field_sql_storage',
      'active' => 1,
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_'.$field_name => array(
              'target_id' => $field_name.'_target_id',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_'.$field_name => array(
              'target_id' => $field_name.'_target_id',
            ),
          ),
        ),
      ),
    ),
  );
  return $field;
}

/**
 * prepares an array for the new content type based on the field collection name
 * strips out 'field' and 'hub' strings from the content type.
 */
function _fc_2_node_prep_fc_fields($fc_name) {

  $content_type_name = strtr($fc_name, array(
    'field_' => '',
    'hub_' => '',
  ));

  $array = array(
    'content_type' => $content_type_name,
    'ct_name' => ucwords(str_replace('_', ' ', $content_type_name)),
    'fc_name' => ucwords(str_replace('_', ' ', $fc_name)),
    'er_field' => $content_type_name . '_er',
  );

  return $array;
}

/**
 * Returns an array of host entities of the specified field collection
 */
function _fc_2_node_get_fc_host_entities($fc_name) {
  $field = field_info_field($fc_name);
  return $field['bundles'];
}
