<?php

/**
 * Set the field collections as selection items
 */
function fc_2_node_form($form, &$form_state) {

  if ( ! isset($form_state['backup_notified']) ) {
    drupal_set_message(t('Please make a backup of your data. Any changes you make are irreversible.'), 'error', FALSE);
    $form_state['backup_notified'] = TRUE;
  }

  // get the counts of each field collection
  $query = db_query('select field_name, COUNT(field_name) as count FROM field_collection_item GROUP BY field_name');
  $result = $query->fetchAllAssoc('field_name');

  $options = array();
  foreach (field_read_fields(array('type' => 'field_collection')) as $field_name) {
    if (!($field_name)){
      $options[0] = 'No Field Collections available.';
    } else {
      if (
        // make sure all these indices are set before we try checking them.
        ! isset($field_name['field_name'])
        || ! isset($result[$field_name['field_name']])
        || ! $count = $result[$field_name['field_name']]->count
      ) {
        $count = 0;
      }
      $options[$field_name['field_name']] = t('@field (@count)',
        array('@field'=>$field_name['field_name'],'@count'=>$count));
    }
  }

  if ( ! count($options) ) {
    $form['notice'] = array(
      '#markup' => t('No field collections currently exist on this site.')
    );
    return $form;
  }

  // uncollapse the fieldset if these are still checked from previous submit
  $collapsed = TRUE;
  if (
    isset($form_state['values']) && (
      $form_state['values']['delete_data']['delete'] === 'delete'
      || $form_state['values']['destroy_data']['destroy'] === 'destroy'
      || $form_state['values']['ignore_orphans']['ignore_orphans'] === 'ignore_orphans'
    )
  ) {
    $collapsed = FALSE;
  }

  $form['data_destruction_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Data Destruction Options'),
    '#collapsible' => TRUE,
    '#collapsed' => $collapsed,
    '#weight' => 10,
  );

  $form['data_destruction_options']['ignore_orphans'] = array(
    '#type' => 'checkboxes',
    '#description' => t('Sometimes there can be orphaned field collections, which are not attached to anything anymore. '
        . 'This option will avoid creating nodes from orphaned field collections. If combined with the \'Delete Data\' option, '
        . 'the orphaned field collection data will be lost entirely.'
      ),
    '#options' => array('ignore_orphans'=>t('Ignore Orphans')),
  );

  $form['data_destruction_options']['delete_data'] = array(
    '#type' => 'checkboxes',
    '#description' => t('The data from the selected Field Collections will be deleted after migration.'),
    '#options' => array('delete'=>t('Delete Field Collection Data')),
  );

  $form['data_destruction_options']['destroy_data'] = array(
    '#type' => 'checkboxes',
    '#description' => t('The selected Field Collection bundles will be removed entirely from the site.'),
    '#options' => array('destroy'=>t('Destroy Field Collection Bundle')),
  );

  $form['selected_fcs'] = array(
    '#title' => t('Field Collections'),
    '#type' => 'checkboxes',
    '#description' => t('The selected Field Collections will be converted to Nodes.'),
    '#options' => $options,
  );

  $form['verbose'] = array(
    '#type' => 'checkboxes',
    '#description' => t('Run in verbose mode (show links to new nodes created).'),
    '#options' => array('verbose'=>t('Verbose Mode')),
    '#weight' => 15,
  );
  if ( isset($form_state['input']['verbose']) && $form_state['input']['verbose']['verbose'] === 'verbose' ) {
    $form['verbose']['#default_value'] = array('verbose');
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Convert to Nodes',
    '#weight' => 20,
  );

  return($form);
}

/**
 * Form validation callback
 * Make sure at least one field collection is selected
 */
function fc_2_node_form_validate($form, &$form_state) {

  // filter out FALSEs
  $selected_fcs = array_filter($form_state['values']['selected_fcs']);

  if (empty($selected_fcs)) {
    form_set_error('selected_fcs', 'No Field Collections selected.');
  }
}

/**
 * Migrated the selected field collections to content types and nodes.
 */
function fc_2_node_form_submit($form, &$form_state) {

  module_load_include('inc', 'fc_2_node', 'fc_2_node.logic');

  // note options for deleting data and destroying field collections
  $options = array();

  if ( $form_state['values']['delete_data']['delete'] === 'delete' ) {
    $options[] = 'delete';
  }
  if ( $form_state['values']['ignore_orphans']['ignore_orphans'] === 'ignore_orphans' ) {
    $options[] = 'ignore_orphans';
  }
  if ( $form_state['values']['destroy_data']['destroy'] === 'destroy' ) {
    $options[] = 'destroy';
  }
  if ( $form_state['values']['verbose']['verbose'] === 'verbose' ) {
    $options[] = 'verbose';
    _fc_2_node_verbose(TRUE);
    drupal_set_message(t('Running in verbose mode.'), 'status', FALSE);
  }

  // filter out FALSEs
  $selected_fcs = array_filter($form_state['values']['selected_fcs']);

  // migrate selected field collections
  foreach ( $selected_fcs as $fc_name ) {
    if ( ! in_array($fc_name, _fc_2_node_processed())) {
      _fc_2_node_migrate_fc($fc_name, $options);
    }
  }

  // delete field collection items if specified
  if ( in_array('delete', $options) ) {
    foreach ( $selected_fcs as $fc_name ) {
      _fc_2_node_delete_fc_items($fc_name);
    }
  }

  // delete field collection itself if specified
  if ( in_array('destroy', $options) ) {
    foreach ( $selected_fcs as $fc_name ) {
      field_delete_field($fc_name);
      field_purge_batch();
      drupal_set_message(t('Removed field collection bundle \'@fc\'.', array('@fc'=>$fc_name)));
    }
  }

  $form_state['rebuild'] = TRUE;
}
