<?php

/**
 * Creates a new node to replace a field collection
 */
function _fc_2_node_add_node($bundle, $fc_name, $fc, $options) {

  $host = $fc->hostEntity();
  if ( is_null($host) && in_array('ignore_orphans', $options) ) {
  // this is an orphaned field collection and the user set to ignore orphans
    // notify the user and skip creating a node.
    drupal_set_message(t('Field collection \'@type\' #@item_id is an orphan with no host, and will be ignored.',
      array('@type'=>$fc->field_name,'@item_id'=>$field_collection->item_id
      )));
    return;
  }

  if ( get_class($host) == "stdClass" ) {
  // this is a node
    // map this field collection to this nid.
    $host_nids = _fc_2_node_item_id_to_host_nid($fc->item_id, $host->nid);
  }
  // create the new node
  $node_wrapper = _fc_2_node_create_node($bundle, $fc->item_id);

  // map any children
  $child_fcs = _fc_2_node_get_child_fc_fields($fc_name);
  foreach ( $child_fcs as $child_fc ) {
    foreach ( $fc->$child_fc as $lang => $values ) {
      foreach ( $values as $value) {
        $host_item_ids = _fc_2_node_item_id_to_host_nid($value['value'],
          $node_wrapper->nid->value());
      }
    }
  }

  // add the field collection values
  _fc_2_node_add_fc_values($node_wrapper, $fc);

  // find the host nid if the FC was already under a node.
  $host_nids = _fc_2_node_item_id_to_host_nid();
  $host_nid = $host_nids[$fc->item_id];
  if ( is_numeric($host_nid) ) {
    $host_node = _fc_2_node_attach_entity_reference(
      $fc_name, $node_wrapper, $host_nid);
    // notify user
    if ( _fc_2_node_verbose() ) {
      _fc_2_node_add_set_verbose_message($host_node, $node_wrapper);
    }
  }
  else {
  // we have an orphaned node and the user didn't specify to ignore them.
    drupal_set_message(t('Node \'@type\' node #@nid "<a href=\'@url\' target=\'_blank\'>@title</a>" '
      . 'is from an orphaned field collection with no host, and therefore cannot be attached to a node.', array(
        '@type'=>$node_wrapper->type->value(),'@nid'=>$node_wrapper->nid->value(),
        '@url'=>'/node/'.$node_wrapper->nid->value(), '@title'=>$node_wrapper->title->value(),
      )));
    $node_wrapper->title = $node_wrapper->title->value() . ' (Orphan)';
    $node_wrapper->save();
    // set the title to orphaned for future reference
  }

  return $node_wrapper;
}

/**
 * Puts on verbose messages of migration details
 */
function _fc_2_node_add_set_verbose_message($host_node, $node_wrapper) {
  $host_node_wrapper = entity_metadata_wrapper('node', $host_node);
  $ids = entity_extract_ids('node', $host_node_wrapper);
  drupal_set_message(t('Created \'@type\' node #@nid, "<a href=\'@url\' target=\'_blank\'>@title</a>"'
    . ' under \'@host_bundle\' node #@host_nid, "<a href=\'@host_url\' target=\'_blank\'>@host_title</a>".',
    array(
      '@type'=>$node_wrapper->type->value(),'@nid'=>$node_wrapper->nid->value(),
      '@url'=>url('node/'.$node_wrapper->nid->value()), '@title'=>$node_wrapper->title->value(),
      '@host_bundle'=>$ids[2]->value(),
      '@host_nid'=>$host_node_wrapper->getIdentifier(),
      '@host_url'=>url('node/'.$host_node_wrapper->getIdentifier()),
      '@host_title'=>$host_node_wrapper->label(),
    )
  ));
}

/**
 * Returns an array of child FCs from the specified parent FC
 */
function _fc_2_node_get_child_fc_fields($fc_name) {
  $output  = array();
  foreach ( field_info_field_map() as $field_name => $info ) {
    if ( $info['type'] == 'field_collection' && in_array($fc_name,$info['bundles']['field_collection_item']) ) {
      $output[] = $field_name;
    }
  }
  return $output;
}

/**
 * Adds the field collections from the original field collection entity to a node
 */
function _fc_2_node_add_fc_values($node_wrapper, $fc) {

  $node_fields = array_keys($node_wrapper->getPropertyInfo());
  $fc_wrapper = entity_metadata_wrapper('field_collection_item', field_collection_item_load(
    $fc->item_id));

  $node = node_load($node_wrapper->nid->value());

  $fc_properties = $fc_wrapper->getPropertyInfo();
  $fc_fields = array_keys($fc_properties);

  $fields = array_intersect($fc_fields, $node_fields);
  // keep track of file fields for later updation
  $files = array();
  foreach ( $fields as $field_name ) {
    // skip fields not prefixed with "field"
    // immutable fields such as 'url' are in the fields array
    if ( substr($field_name, 0, 5) == "field" ) {
      $field_info = $fc_wrapper->$field_name->info();
      try {
        // keep track of file fields for later updation
        if ( isset($fc_properties[$field_name]['property info']['image'])
          || isset($fc_properties[$field_name]['property info']['file']) ) {
          $files[] = $node_wrapper->{$field_name};
        }
        foreach ( $fc->$field_name as $lang => $values ) {
          foreach ( $values as $index => $value ) {
            if ( isset($fc->{$field_name}[$lang][$index]) ) {
              $node->{$field_name}[$lang][$index] = $fc->{$field_name}[$lang][$index];
            }
          }
        }
      } catch ( exception $e ) {
        drupal_set_message(t('Caught exception copying value from field \'@field\': \'@msg\'',
          array('@field'=>$field_name, '@msg'=> $e->getMessage())));
      }
    }
  }
  node_save($node);
  _fc_2_node_update_file_usage($files, $node_wrapper->nid->value());
}

/**
 * Add file usage entry for each file in replacement node
 */
function _fc_2_node_update_file_usage($files, $nid) {
  foreach ( $files as $field ) {
    if ( 'EntityListWrapper' != get_class($field) ) {
      $field = array($field);
    }
    foreach ( $field as $file ) {
      if ( ( $file = (object)$file->value() ) && $file->fid) {
        file_usage_add($file, 'file', 'node', $nid);
      }
    }
  }
}

/**
 * Creates a new node of the specified bundle
 */
function _fc_2_node_create_node($bundle, $fc_id) {

  $node = new stdClass();
  // Can't really make much more of a title than this.
  $node->title = "Item " . $fc_id;
  $node->type = $bundle;
  node_object_prepare($node); // Sets some defaults. Invokes hook_prepare() and hook_node_prepare().
  $node->language = 'en'; // Or e.g. 'en' if locale is enabled
  $node->uid = 0;
  $node->status = 1; //(1 or 0): published or not
  $node->promote = 0; //(1 or 0): promoted to front page

  $node = node_submit($node); // Prepare node for saving
  node_save($node);
  return entity_metadata_wrapper('node', $node);
}

/**
 * Populates the entity relation field of the original host node
 * associating it to the new node of the new content type from the migrated field collection.
 */
function _fc_2_node_attach_entity_reference( $fc_name, $new_node_wrapper, $host_nid ) {

  $host_node = node_load($host_nid);

  // get the name of the ER field on the host content type
  $prep = _fc_2_node_prep_fc_fields($fc_name);
  $er_field = $prep['er_field'];
  // put the target nid on the ER field
  if ( ! isset($host_node->{$er_field}) || ! is_array($host_node->{$er_field}) || empty($host_node->{$er_field}) ) {
    $host_node->$er_field = array();
    $host_node->{$er_field}[$host_node->language][0] = array('target_id'=>$new_node_wrapper->nid->value());
  }
  elseif ( is_array($host_node->{$er_field}[$host_node->language]) ) {
    $host_node->{$er_field}[$host_node->language][] = array('target_id'=>$new_node_wrapper->nid->value());
  }
  node_save($host_node);
  return $host_node;
}

/**
 * Stores a mappping of item ids to nids
 */
function _fc_2_node_item_id_to_host_nid($item_id = NULL, $host_nid = NULL) {
  static $mappings = array();
  if ( ! is_null($item_id) && ! is_null($host_nid) ) {
    $mappings[$item_id] = $host_nid;
  }
  return $mappings;
}
